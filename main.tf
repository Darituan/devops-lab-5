module "lab05" {
  source = "terraform-aws-modules/ec2-instance/aws"

  name = "${local.name}-${local.author}-${terraform.workspace}"

  ami                         = data.aws_ami.ubuntu.id
  ignore_ami_changes          = true
  instance_type               = local.instance_type
  availability_zone           = data.aws_subnet.selected.availability_zone
  subnet_id                   = data.aws_subnet.selected.id
  vpc_security_group_ids      = [module.security_group.security_group_id]
  associate_public_ip_address = true
  key_name                    = module.key_pair.key_pair_name

  user_data_base64            = base64encode(local.user_data)
  user_data_replace_on_change = false

  root_block_device = [
    {
      encrypted   = true
      volume_type = local.storage_volume_type
      throughput  = local.storage_throughput
      volume_size = local.storage_volume_size
    }
  ]

  tags = local.tags
}


module "key_pair" {
  source = "terraform-aws-modules/key-pair/aws"

  key_name           = "${local.author}-${local.name}"
  create_private_key = true
}

module "security_group" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 4.0"

  name        = "${local.author}-${local.name}-${terraform.workspace}"
  description = "Security group for EC2 instance"
  vpc_id      = data.aws_vpc.default.id

  ingress_cidr_blocks = ["0.0.0.0/0"]
  ingress_rules       = ["http-80-tcp", "https-443-tcp", "ssh-tcp"]
  egress_rules        = ["all-all"]

  tags = local.tags
}