locals {
  author = "nedosieka"

  name   = "lab05"
  region = "eu-west-2"

  storage_volume_type = "gp3"
  storage_volume_size = 30
  storage_throughput  = 200

  instance_type = "t2.micro"

  user_data = <<-EOT
    #cloud-config
    package_update: true
    package_upgrade: true
    package_reboot_if_required: true
    fqdn: "${local.name}-${terraform.workspace}"
    swap:
      filename: /swapfile
      size: 8G
      maxsize: 8G
    mounts:
      - ["swap", "none", "swap", "sw", "0", "0"]
    packages:
      - ca-certificates
      - curl
      - amazon-ecr-credential-helper
    runcmd:
      - install -m 0755 -d /etc/apt/keyrings
      - curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
      - chmod a+r /etc/apt/keyrings/docker.asc
      - echo \
        "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
        $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
        sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
      - apt-get update && apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
    write_files:
      - path: /etc/crontab
        content:
          @daily /usr/bin/docker image prune -f -a --filter "until=24h" > /dev/null
        append: true
    final_message: "The system is finally up, after $UPTIME seconds"
  EOT

  tags = {
    Environment = terraform.workspace
    Name        = "${local.name}-${local.author}"
    ManagedBy   = "terraform"
    Repository  = ""
  }
}